package com.security.demo.common.config.mybatis;

import com.security.demo.common.config.security.WebSecurityConfig;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/*
* 类描述：开启mybatis的支持
* @auther 逍遥若峰
* @create 2019年6月3日17:19:27
*/
@Configuration
@MapperScan("com.security.demo.*.dao")
public class MyBatisConfig  {


}
