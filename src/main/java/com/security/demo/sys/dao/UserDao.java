package com.security.demo.sys.dao;




import com.security.demo.sys.entity.User;

import java.util.List;

/**
 *@author linzf
 **/
/**
 *@author linzf
 **/
public interface UserDao {

    /**
     * 功能描述：根据账号来获取用户信息
     * @param login
     * @return
     */
    User findByLogin(String login);


}